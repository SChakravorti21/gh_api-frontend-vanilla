// Define the base URL at which we will be making requests.
// All web APIs have a base URL, and specific queries will require
// making a request to a specific path.
const baseUrl = "https://api.github.com";

// Get a handle to the search input on the page. This allows
// us to get the search term and make a request to the 
// GitHub API
const searchInput = document.querySelector("#search-input");
const resultsDisplay = document.querySelector("#search-results");

// Manually handle submitting the form. 
document.forms[0].onsubmit = (event) => {
    // Usually forms contain an "action" attribute that automatically 
    // decides what the form should do (usually ends up in a redirect).
    event.preventDefault();

    // Construct an API request to fetch information from the
    // GitHub API
    const repoRequest = new Request(`${baseUrl}/search/repositories?q=${searchInput.value}`);
    searchInput.value = "";
    resultsDisplay.innerHTML = "";

    // Fetch the data, then convert it to a JSON object
    // Finally, display the results. Proper error-handling is ommitted for brevity.
    fetch(repoRequest)
        .then(response => response.json())
        .then(response => constructCards(response.items))
        .then((cards) => displayCards(cards))
        .catch((reason) => {
            console.log(reason);
        });
}

let constructCards = (repositories) => {
    // For each repository that was found, we
    // will create a card containing the creator's
    // image, the repository name (linked to the actual repo),
    // and the short description of the repository.
    return repositories.map((repository) =>
        `<div class="card repo-card mb-4">
            <img class="avatar-img" src=${repository.owner.avatar_url} />
            <div class="card-body">
                <a class="card-title" href=${repository.html_url}
                    target="_blank">${repository.full_name}</a>
                <p class="card-text">${repository.description}</p>
            </div>
        </div>`
    );
}

let displayCards = (cards) => {
    // For each card, we need to to create a new
    // div element as a row to contain the card.
    // We'll then insert the card into the row and add the row
    // to the list of results.
    cards.forEach(card => {
        const cardNode = document.createElement("div");
        cardNode.classList.add("row");
        cardNode.innerHTML = card;
        resultsDisplay.appendChild(cardNode);
    });
}